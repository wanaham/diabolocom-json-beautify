# Afficher le contenu d’un JSON de manière élégante.

Source du JSON : https://raw.githubusercontent.com/substack/node-browserify/master/package.json
 
[ ] - L'utilisateur rentre l'URL du JSON dans un champ et l'application affiche un tableau au format HTML avec 3 colonnes.
 
[x] - La première colonne contient la clé.
 
[x] - La deuxième colonne donne un aperçu de la valeur comme suit :
- string : Afficher les 10 premiers caractères
- array : Afficher la taille
- object : Afficher le nombre de clés
 
[x] - La troisième colonne contient le type de la valeur (string, array, object).
 
Contraintes :
[x] - - N'afficher que les 10 premières clés dans l'ordre alphabétique
[x] - - Seule la libraire jQuery est autorisée
 
[x] - Appliquer un joli CSS au tableau.

Durée : 1h30

# How to work with source

1 install nodejs & npm
2 execute npm install grunt -g
3 execute npm install bower -g
4 bower install && npm install && grunt watch
5 Have fun !