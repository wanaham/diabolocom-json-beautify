module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		cssmin: {
            target: {
                files: {
                    'built/css/main.min.css': [
                        'bower_components/bootstrap/dist/css/bootstrap.min.css',
                        'bower_components/animate.css/animate.min.css',
                        'src/custom.css'
                    ]
                }
            },
			options: {
				'banner': null,
				'keepSpecialComments': '*',
				'report': 'min'
			}
		},
        watch: {
            scripts: {
                files: 'src/*.js',
                tasks: ['uglify'],
                options: {
                    interrupt: true
                }
            },
            styles: {
                files: 'src/*.css',
                tasks: ['cssmin'],
                options: {
                    interrupt: true
                }
            }
        },
		uglify: {
            target: {
                files: {
                    'built/js/main.min.js': [
                        'bower_components/jquery/dist/jquery.min.js',
                        'src/custom.js'
                    ]
                }
            },
			options: {
				'mangle': {},
				'compress': {},
				'beautify': false,
				'expression': false,
				'report': 'min',
				'sourceMap': false,
				'sourceMapName': undefined,
				'sourceMapIn': undefined,
				'sourceMapIncludeSources': false,
				'enclose': undefined,
				'wrap': undefined,
				'exportAll': false,
				'preserveComments': undefined,
				'banner': '(function(){',
				'footer': '})();'
			}
		}
	});

    grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.registerTask('default', ['cssmin', 'uglify']);
};